const express = require('express');
const app = express();


let indexRouter = require('./routes/index');
let port = 3000;


app.use('/index', indexRouter);


app.get('/', (req, res) => {
    res.send('nodejs server');
});


app.listen(port, () => console.log(`Example app listening on port ${port}!`));

module.exports = app;


