let express = require('express');
let router = express.Router();
let indexCtrl = require('../controllers/index.js');

/* GET home page. */
router.get('/', function(req, res, next) {
    indexCtrl.getHomePage(function(err,data){
        res.send(data);
    })
});

module.exports = router;
